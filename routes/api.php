<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {
    //    Route::resource('task', 'TasksController');

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_api_routes
});

Route::group(['prefix'=>'v1', 'middleware'=>'cors'], function(){
	Route::resource('actions', 'ActionsController');
	Route::resource('users', 'UsersController');
	Route::resource('contacts', 'UsersContactsController');
	Route::resource('categories', 'CategoriesController');
	Route::resource('community', 'CommunityController');
	Route::resource('userlikes', 'UserLikesController');
	//Route::post('ferias/filtros', 'FeriasController@filtros');
});
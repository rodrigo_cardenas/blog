<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/mail/reply/{id}/{score}', 'MailController@reply');
Route::post('/mail/comment/{id}', 'MailController@comment');


Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
    Route::get('/user/profile/{id}', 'UsersController@profile');
    Route::get('/home/global', 'HomeController@global');
    Route::get('/home/community', 'HomeController@community');
    Route::post('/home/user_filters', 'HomeController@user_filters');
    Route::patch('/user/update/{id}', 'UsersController@update')->name('user.update');
    Route::patch('/communities/update/{id}', 'CommunityController@update')->name('communities.update');
    Route::get('/contacts/dash/{id}', 'UsersContactsController@UserContacts');
    Route::get('/communities/dash/{id}', 'CommunityController@UserCommunities');
    Route::get('/actions/dash/{id}', 'ActionsController@UserActions');
    Route::get('/actions/list/{id}', 'ActionsController@UserActionsList');
    Route::get('/actions/contact/{id}/{contact}', 'ActionsController@UserContactActions');
    Route::get('/actions/delete/{id}', 'ActionsController@delete_action');
    Route::get('/contacts/delete/{id}', 'UsersContactsController@delete');
    Route::get('/communities/delete/{id}', 'CommunityController@delete');
    Route::post('/actions/filter/', 'ActionsController@FilterActions')->name('actions.filter');
    Route::patch('/actions/update/', 'ActionsController@updateAction')->name('actions.update');
    Route::get('/communities/new/', 'CommunityController@new');
    Route::get('/actions/new/', 'ActionsController@new');
    Route::get('/contacts/new/', 'UsersContactsController@new');
    Route::get('/community/detail/{id}', 'CommunityController@detail');
});

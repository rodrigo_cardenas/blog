<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
@section('htmlheader_title', 'Community')
    @include('adminlte::layouts.partials.htmlheader')
@show
<body class="skin-green sidebar-mini">
    <div id="app" v-cloak>
        <div class="wrapper">

        @include('adminlte::layouts.partials.mainheader')

        @include('adminlte::layouts.partials.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @section('contentheader_title', '')
            @section('contentheader_description', '')
            @include('adminlte::layouts.partials.contentheader')

            <!-- Main content -->
            <section class="content">
                <!-- /.row -->
                <div class="row">
                    <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        <h3 class="box-title"><strong>Community</strong></h3>
                        <div class="pull-right">
                            <h4 > <i class="fa fa-group"></i> 1 </h4>
                        </div>

                        <div class="box-tools">
                            
                        </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            {!! Form::open(['class'=>'form-horizontal', 'route' => ['communities.update', $communities->id], 'method' => 'patch']) !!}
                            <div class="form-group">
                                <input type="hidden" name="view" value="1">
                                <label for="inputName" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                <input type="text" required value="{{ $communities->name }}" class="form-control" name="name" id="inputName" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="view" value="1">
                                <label for="inputName" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                <input type="text" required value="{{ $communities->description }}" class="form-control" name="description" id="inputDescription" placeholder="Description">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputLastname" class="col-sm-2 control-label">Category</label>

                                <div class="col-sm-10">
                                    <select class="form-control pull-right" required name="category" >
                                        @foreach ($communitycategories as $category)
                                            <option {{ $category->id==$communities->category ? 'selected' : '' }} value="{{ $category->id }}"> {{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">Created At</label>

                                <div class="col-sm-10">
                                <span>{{ $communities->created_at }}</span>                                </div>
                            </div>
                            @if($communities->created_by==Auth::id())
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger">Save</button>
                                </div>
                            @endif
                            <!-- <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Save</button>
                                </div>
                            </div> -->
                            {!! Form::close() !!}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    </div>
                </div>
                
                
                
                @yield('main-content')
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        @include('adminlte::layouts.partials.controlsidebar')

        @include('adminlte::layouts.partials.footer')
        </div>
    </div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>

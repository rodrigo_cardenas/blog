<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
@section('htmlheader_title', 'Connections')
    @include('adminlte::layouts.partials.htmlheader')
@show




<body class="skin-green sidebar-mini">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @section('contentheader_title', 'New Connection')
        @section('contentheader_description', '')
        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title"> </h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                            {!! Form::open(['class'=>'form-horizontal', 'route' => ['community.store'], 'method' => 'post']) !!}
                            <div class="form-group">
                              <input type="hidden" name="view" value="1">
                              <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                              <input type="hidden" name="created_by" value="{{ Auth::id() }}">
                              <label for="inputName" class="col-sm-2 control-label">Name *</label>
                              <div class="col-sm-10">
                                    <input type="text" class="form-control pull-right" name="name" id="name">
                              </div>
                            </div>
                            
                            <div class="form-group">
                              <label for="inputLastName" class="col-sm-2 control-label">Description</label>
          
                              <div class="col-sm-10">
                                  <textarea class="form-control" name="description" id="" cols="30" rows="3"></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputLastName" class="col-sm-2 control-label">Category</label>
          
                              <div class="col-sm-10">
                                  <select class="form-control pull-right" name="category" id="">
                                      @foreach ($CommunityCategoriess as $category)
                                      <option value="{{ $category->id }}">{{ $category->title }}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputLastName" class="col-sm-2 control-label">Invite Contact </label>
          
                              <div class="col-sm-10">
                                  <select name="invite[]" id="" class="select2" multiple>
                                    @foreach ($contacts as $contact)
                                    <option value="{{ $contact->email }}">{{ $contact->first_name }} {{ $contact->last_name }}</option>
                                    @endforeach
                                  </select>
                              </div>
                            </div>
                            
                            <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                              <input id="type" name="type" type="checkbox"  checked> Public (Anyone Can Join)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                            </div>
                          {!! Form::close() !!}
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        </div>
                    </div>
            
            
            @yield('main-content')
          
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    
    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>

<script> 
    function score_check(value) {
        $('#score').val(value);
        console.log(value);
    }
    function actionNew(id, name) {
        $('#contact_tittle').html(name);
        $('#user_id').val({{Auth::id()}});
        $('#contact_id').val(id);
        console.log('id: '+id);
    }
    $( "#energy_multiplier" ).change(function() {
        var hms = $('#total_time').val();   // your input string
        var a = hms.split(':'); // split it at the colons
        var seconds = (+a[0]) * 60  + (+a[1]) ;
        var seconds_c = seconds * 0.5;  
        var total_money = $('#total_money').val();
        var total_money_c = total_money * 3;  
        var energy_multiplier = $('#energy_multiplier').val();
        var total_amount = (seconds_c + total_money_c) * energy_multiplier;
        
        console.log('seconds*0.5 :'+seconds_c);
        console.log('total_money*3 :'+total_money_c);
        console.log('energy_multiplier :'+energy_multiplier);
        console.log('total_amount :'+total_amount);
        $('#total_amount').val(total_amount);

    });
    $( "#total_money" ).change(function() {
        var hms = $('#total_time').val();   // your input string
        var a = hms.split(':'); // split it at the colons
        var seconds = (+a[0]) * 60  + (+a[1]) ;
        var seconds_c = seconds * 0.5;  
        var total_money = $('#total_money').val();
        var total_money_c = total_money * 3;  
        var energy_multiplier = $('#energy_multiplier').val();
        var total_amount = (seconds_c + total_money_c) * energy_multiplier;
        
        console.log('seconds*0.5 :'+seconds_c);
        console.log('total_money*3 :'+total_money_c);
        console.log('energy_multiplier :'+energy_multiplier);
        console.log('total_amount :'+total_amount);
        $('#total_amount').val(total_amount);

    });
    $( "#total_time" ).change(function() {
        var hms = $('#total_time').val();   // your input string
        var a = hms.split(':'); // split it at the colons
        var seconds = (+a[0]) * 60  + (+a[1]) ;
        var seconds_c = seconds * 0.5;  
        var total_money = $('#total_money').val();
        var total_money_c = total_money * 3;  
        var energy_multiplier = $('#energy_multiplier').val();
        var total_amount = (seconds_c + total_money_c) * energy_multiplier;
        
        console.log('seconds*0.5 :'+seconds_c);
        console.log('total_money*3 :'+total_money_c);
        console.log('energy_multiplier :'+energy_multiplier);
        console.log('total_amount :'+total_amount);
        $('#total_amount').val(total_amount);

    });

    $('#score_now').change(function() {
        if($(this).is(":checked")) {
            $('#div_score').css("display", '');
        }else{
            $('#div_score').css("display", 'none');
        }
    });
</script>
</html>

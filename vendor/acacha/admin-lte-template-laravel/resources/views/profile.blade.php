<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
@section('htmlheader_title', 'Profile')
    @include('adminlte::layouts.partials.htmlheader')
@show

<body class="skin-green sidebar-mini">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @section('contentheader_title', 'User')
        @section('contentheader_description', 'Profile')
        @include('adminlte::layouts.partials.contentheader')

       
        <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary box-green">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{ Gravatar::get($user->email) }}" alt="User profile picture">

              <h3 class="profile-username text-center">{{ $user->name }}</h3>

              <p class="text-muted text-center">{{ $user->email }}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <p><b>Actions</b> <a class="pull-right">{{ $actions_number }}</a></p>
                  <p><b>Score Average</b> <a class="pull-right">{{ $actions_average }}</a></p>
                  {{--  <p><b>Total Giving Amount</b> <a class="pull-right">{{ $total_givingamount }}</a></p>  --}}
                  <p><b>Springship Points</b> <a class="pull-right">{{ $springship_points }}</a></p>
                </li>
                
                <li class="list-group-item">
                  <b>Contacts</b> <a class="pull-right">{{ $contacts_number }}</a>
                </li>
                <li class="list-group-item">
                  <b>Communities</b> <a class="pull-right">{{ $comunnity_number }}</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li ><a href="#settings" data-toggle="tab">Settings</a></li>
              <li class="active"><a href="#password" data-toggle="tab">Password</a></li>
            </ul>
            <div class="tab-content">
              
              <div class=" tab-pane" id="settings">
                {{--  <form class="form-horizontal">  --}}
                {!! Form::open(['class'=>'form-horizontal', 'route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                  <div class="form-group">
                    <input type="hidden" name="view" value="1">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" value="{{ $user->name }}" class="form-control" name="name" id="inputName" placeholder="Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputLastname" class="col-sm-2 control-label">Lastname</label>

                    <div class="col-sm-10">
                      <input type="text" value="{{ $user->last_name }}" class="form-control" name="last_name" id="inputLastname" placeholder="Lastname">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="email" value="{{ $user->email }}" class="form-control" name="email" id="inputEmail" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label">Phone</label>

                    <div class="col-sm-10">
                        <input type="phone" value="{{ $user->phone }}" class="form-control" name="phone" id="inputPhone" placeholder="Phone">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label">Birth Date</label>

                    <div class="col-sm-10">
                        <input type="text" value="{{ $user->birthdate }}" class="form-control datepicker" name="birthdate" id="inputBirthdate" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-10">
                        <input id="autocomplete" value="{{ $user->address }}" class="form-control" name="address" placeholder="Enter your address" onFocus="geolocate()" type="text"></input>
                    </div>
                  </div>
                  <table class="table" id="address" style="display:none;">
                      <tr>
                        <td class="">Street address</td>
                        <td class="slimField"><input class="field" id="street_number"
                              disabled="true"></input></td>
                        <td class="wideField" colspan="2"><input class="field" id="route"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="">City</td>
                       
                        <td class="wideField" colspan="3"><input class="field" id="locality"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="">State</td>
                        <td class="slimField"><input class="field"
                              id="administrative_area_level_1" disabled="true"></input></td>
                        <td class="">Zip code</td>
                        <td class="wideField"><input class="field" id="postal_code"
                              disabled="true"></input></td>
                      </tr>
                      <tr>
                        <td class="">Country</td>
                        <td class="wideField" colspan="3"><input class="field"
                              id="country" disabled="true"></input></td>
                      </tr>
                    </table>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Save</button>
                    </div>
                  </div>
                {!! Form::close() !!}
                {{--  </form>  --}}
              </div>
              <!-- /.tab-pane -->
              <div class="active tab-pane" id="password">
                  
                  @if (session('error'))
                      <div class="alert alert-danger">
                          {{ session('error') }}
                      </div>
                  @endif
                  @if (session('success'))
                      <div class="alert alert-success">
                          {{ session('success') }}
                      </div>
                  @endif
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                {{--  <form class="form-horizontal">  --}}
                {!! Form::open(['class'=>'form-horizontal', 'route' => ['user.update', $user->id], 'method' => 'patch']) !!}
                <div class="form-group col-sm-12">
                    <input type="password" class="form-control" required placeholder="Current Password" name="current_password"/>
                </div>
                <div class="form-group col-sm-12">
                    <input type="password" class="form-control" required placeholder="New Password" name="password"/>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-sm-12">
                    <input type="password" class="form-control" required placeholder="Confirm New Password" name="password_confirmation"/>
                </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Save</button>
                    </div>
                  </div>
                {!! Form::close() !!}
                {{--  </form>  --}}
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
<script>
  // This example displays an address form, using the autocomplete feature
  // of the Google Places API to help users fill in the information.

  // This example requires the Places library. Include the libraries=places
  // parameter when you first load the API. For example:
  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

  var placeSearch, autocomplete;
  var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
  };

  function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
  }

  function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
      document.getElementById(component).value = '';
      document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      if (componentForm[addressType]) {
        var val = place.address_components[i][componentForm[addressType]];
        document.getElementById(addressType).value = val;
      }
    }
  }

  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
  }
</script>
//floating button 
@section('floating_button')
    @include('adminlte::layouts.floating_button')
@show
</html>

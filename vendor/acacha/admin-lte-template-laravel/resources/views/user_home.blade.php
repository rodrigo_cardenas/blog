<!DOCTYPE html>

<html lang="en">
@section('htmlheader')
@section('htmlheader_title', 'actions')
    @include('adminlte::layouts.partials.htmlheader')
@show


<body class="skin-green sidebar-mini">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        

        <!-- Main content -->
        <section class="content">
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12" style="padding-left:0px; padding-right:0px;">
                    <div class="box">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li style="width:30%;text-align:center;" onclick="load_global();" class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-globe fa-3x"></i></a></li>
                                <li style="width:30%;text-align:center;" onclick="load_community();" class="pull-center"><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-users fa-3x"></i></a></li>
                                <li style="width:33%;text-align:center;" class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="true"><i class="fa fa-user fa-3x"></i></a></li>
                                
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="tab_1">
                                    {{--  @section('global')
                                        @include('adminlte::layouts.global_tab')
                                    @show  --}}
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    {{--  @section('community')
                                        @include('adminlte::layouts.community_tab')
                                    @show  --}}
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane active" id="tab_3">
                                    @section('tab')
                                        @include('adminlte::layouts.user_tab')
                                    @show
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show
@section('floating_button')
    @include('adminlte::layouts.floating_button')
@show
</body>
<script>
    function load_global(){
        $.get( "{{ url('home/global') }}", function( data ) {
            $('#tab_1').html(data);
        });
        
    }
    function load_community(){
        $.get( "{{ url('home/community') }}", function( data ) {
            $('#tab_2').html(data);
        });
        
    }
</script>
</html>

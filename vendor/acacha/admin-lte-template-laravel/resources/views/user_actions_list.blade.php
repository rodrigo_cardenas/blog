<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
@section('htmlheader_title', 'actions')
    @include('adminlte::layouts.partials.htmlheader')
@show


<body class="skin-green sidebar-mini">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @section('contentheader_title', '')
        @section('contentheader_description', '')
        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    <h3 class="box-title">My Actions</h3>

                    <div class="box-tools">
                        <a href="actions/new" class="btn btn-default" ><i class="fa fa-plus"></i>New action</a>
                       
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        @section('tab')
                            @include('adminlte::layouts.user_tab')
                        @show
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                </div>
            </div>
            {{--  new self note modal  --}}
            <div class="modal fade" id="modal-note">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Self note</h4>
                    </div>
                    <div class="modal-body">
                            {!! Form::open(['class'=>'form-horizontal', 'route' => ['actions.update'], 'method' => 'patch']) !!}
                            <div class="form-group">
                              <input type="hidden" name="view" value="1">
                              <input type="hidden" name="id" id="action_id" >
                              <div class="col-sm-12">
                                  <textarea name="self_note" class="form-control pull-right" id="" cols="30" rows="10"></textarea>
                              </div>
                            </div>
                            
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                            </div>
                          {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                    </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
            <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            
            
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
<script>
    function notesticky(id) {
        $('#user_id').val({{Auth::id()}});
        $('#action_id').val(id);
        console.log('id: '+id);
    }
</script>
</html>

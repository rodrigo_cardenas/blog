<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcJQ4Su2uBvuJ2ZNFUwio2uoH-_-wXmKs&libraries=places&callback=initAutocomplete"
        async defer></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
      <script>
            //Initialize Select2 Elements
            $('.select2').select2({ width: '100%'  });      
            //Date range picker
            $('#reservation').daterangepicker({
                  locale: {
                        format: 'YYYY/MM/DD'
                    }
            })
            //datatables
            $('.datatable').DataTable({
                  responsive: true
            });
            //checkbox
            $('.input[type="checkbox"]').iCheck()
            //Date picker
            $('.datepicker').datepicker({
                  format: 'yyyy-mm-dd',
                  autoclose: true
            })
             //Timepicker
            $('.timepicker').timepicker({
                  showMeridian: false,
                  showInputs: false,
                  maxHours: 96,
            })

            
            $('.datetimepicker').datetimepicker({
                  format: 'YYYY-MM-DD HH:mm'
              });
      </script>

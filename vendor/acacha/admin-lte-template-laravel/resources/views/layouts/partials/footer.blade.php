<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Developed by <a href="http://cardplaz.esy.es/portafolio">CardPlaz</a>. {{ trans('adminlte_lang::message.seecode') }} <a href="https://github.com/rodrigocardenas">Github</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="http://springship.org">Springship.org</a>.</strong> {{ trans('adminlte_lang::message.createdby') }} <a href="http://acacha.org/sergitur">Andrew Reid</a>. 
</footer>

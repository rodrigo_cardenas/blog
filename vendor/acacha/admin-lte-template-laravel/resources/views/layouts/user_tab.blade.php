<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li style="width:48%;text-align:center;" class="active"><a href="#list" data-toggle="tab" aria-expanded="false">List</a></li>
        <li style="width:48%;text-align:center;" class="pull-center"><a href="#charts" data-toggle="tab" aria-expanded="false">Charts</a></li>
    </ul>
    <div class="tab-content" style="background: #edf0f6;">
        <div class="tab-pane active" id="list">
                <div class="box box-default collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><a data-widget="collapse"><i class="fa fa-filter"></i> Filters</a> </h3>
        
                        <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                        </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="display: none;">
                           <form method="POST" id="filters_form" >
                            <!-- Date range -->
                            <div class="form-group col-md-6">
                              <label>Date range:</label>
              
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input name="date" type="text" class="form-control pull-right" id="reservation">
                              </div>
                              <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                            <!-- Community -->
                            <div class="form-group col-md-6">
                              <label>Community:</label>
              
                              <div class="input-group">
                                <select name="community" id="community" class="form-control select2" multiple="multiple" data-placeholder="Select a State" style="width:100% !important;" >
                                    <option value="All" selected>All</option>
                                    <option value="Detroit">Detroit</option>
                                    <option value="Detroit">Grosse Pointe Park</option>
                                </select>
                              </div>
                              <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                            <!-- Springship Points -->
                            <div class="form-group col-md-2">
                              <label>Springship Points:</label>
              
                              <div class="input-group">
                                <select name="s_points" id="s_points" class="form-control select2" data-placeholder="Select a State" style="width:100% !important;" >
                                    <option value="All" selected>All</option>
                                    <option value="10"> < 10 </option>
                                    <option value="50"> < 50 </option>
                                    <option value="100"> < 100 </option>
                                    <option value="1000"> < 1000 </option>
                                </select>
                              </div>
                              <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                            <!-- Total Giving Amount -->
                            <div class="form-group col-md-2">
                              <label>Total Giving Amount:</label>
              
                              <div class="input-group">
                                <select name="total_ga" id="total_ga" class="form-control select2" data-placeholder="Select a State" style="width:100% !important;" >
                                    <option value="All" selected>All</option>
                                    <option value="10"> < 10 </option>
                                    <option value="50"> < 50 </option>
                                    <option value="100"> < 100 </option>
                                    <option value="1000"> < 1000 </option>
                                </select>
                              </div>
                              <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                            <!-- Score -->
                            <div class="form-group col-md-2">
                              <label>Score:</label>
              
                              <div class="input-group">
                                <select name="score" id="score" class="form-control select2" data-placeholder="Select a State" style="width:100% !important;" >
                                    <option value="All" selected>All</option>
                                    <option value="0"> < 0 </option>
                                    <option value="1"> < 1 </option>
                                    <option value="2"> < 2 </option>
                                    <option value="3"> < 3 </option>
                                </select>
                              </div>
                              <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                            <!-- Comments -->
                            <div class="form-group col-md-2">
                              <label>Comments:</label>
              
                              <div class="input-group">
                                    <div class="checkbox">
                                        <label>
                                            <input id="comments" name="comments" checked type="checkbox"> Yes
                                        </label>
                                    </div>
                              </div>
                              <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a id="submit_filters" onclick="filters();" class="btn btn-primary">Find</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            @foreach ($actions as $key=>$action)
            <!-- Box Comment -->
            <div class="box box-widget">
                <div class="box-header with-border">
                <div class="user-block">
                    <img class="img-circle" src="{{ Gravatar::get($action->user_email) }}" alt="User Image">
                    {{--  <span class="username"><a href="#">{{ $action->user_id == Auth::id() ? 'Me' : $action->user_name }} gave to <a href="{{ url('/actions/contact') }}/{{ $action->user_id }}/{{ $action->id }}">{{ $action->contact_email == Auth::user()->email ? 'Me' : $action->contact_name }} </a></a></span>  --}}
                    <span class="username">{{ $action->contact_name }} was given to by {{ $action->user_name }} <i class="fa {{ ($action->private==0) ? 'fa-globe' : 'fa-lock'}}"></i></span>
                    <span class="description">Springship Points: {{ $action->springship_points }}pts - {{ \Carbon\Carbon::parse($action->date)->diffForHumans() }}</span>
                </div>
                <!-- /.user-block -->
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" title="Mark as read">
                    <i class="fa fa-ellipsis-h"></i></button>  
                    <ul class="dropdown-menu pull-left" role="menu" style="margin-left:-140px !important;">
                        {{--  <li><a href="#"><i class="fa fa-eye"></i> Details</a></li>  --}}
                        <li><a href="mailto:andrewreid89@gmail.com?Subject=Report%20Action"><i class="fa fa-exclamation-circle"></i> Report</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/actions/delete') }}/{{ $action->id }}" onclick="return confirm('Are you sure you want to delete this connection?')"><i class="fa fa-trash"></i> Delete</a></li>
                      </ul>
                    {{-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> --}}
                    </button>
                </div>
                <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                {{--  <img class="img-responsive pad" src="../dist/img/photo2.png" alt="Photo">  --}}

                <h4>{{ $action->description }}</h4>
                <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button>
                <input type="text" style="display:none;" name="like_status" id="like_status" value="{{ isset($action->like[0]->id) ? $action->like[0]->id : '0' }}">
                <button type="button" class="btn btn-default btn-xs" onclick="liked({{ $action->id }}, {{ Auth::id() }}, {{ isset($action->like[0]->id) ? $action->like[0]->id : 0 }}) "><i id="like-{{ $action->id }}" class="fa {{ isset($action->like[0]->id) ? 'fa-heart' : 'fa-heart-o' }}"></i> </button>
                <span class="pull-right text-muted" id="s_total_likes-{{ $action->id }}">{{ $action->total_likes }} likes </span>
                <input type="text" name="total_likes" id="total_likes" style="display:none;" value="{{ $action->total_likes }}">
                </div>
                <!-- /.box-body -->
                <div class="box-footer box-comments">
                <div class="box-comment">
                    @if (!empty($action->score))
                    <!-- User image -->
                    <img class="img-circle img-sm" src="{{ Gravatar::get($action->contact_email) }}" alt="User Image">
                    <div class="comment-text">
                        <span class="username">
                                {{ $action->first_name }} {{ $action->last_name }}
                            <span class="text-muted pull-right">8:03 PM Today</span>
                        </span><!-- /.username -->
                        <h4><a href="#" title="{{$action->value}} points">{{$action->emoji}}</a></h4>
                    {{$action->contact_comment}}
                    </div>
                    @endif
                @if (!empty($action->self_note))
                    <!-- User image -->
                    <img class="img-circle img-sm" src="{{ Gravatar::get($action->user_email) }}" alt="User Image">
                    <div class="comment-text">
                        <span class="username">
                                Me
                            <span class="text-muted pull-right">8:03 PM Today</span>
                        </span><!-- /.username -->
                    {{$action->self_note}}
                    </div>
                @endif
                    
                    <!-- /.comment-text -->
                </div>
                <!-- /.box-comment -->
                
                </div>
            </div>
            <!-- /.box -->
                    
            @endforeach
            @forelse ($actions as $action)
                @empty
                <p>No actions</p>
            @endforelse
            
        <!-- /.tab-pane -->
        </div>
        <div class="tab-pane" id="charts">
            <div id="chart_div" style="width: 90%; height: 20%;"></div>
            <br>
            <div class="col-md-5 col-sm-offset-3">
            <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table class="datatable table table-hover">
                            <tr>
                                <th>Springship Points</th>
                                <td>2</td>
                            </tr>
                            <tr>
                                <th>Total Giving Amount</th>
                                <td>2</td>
                            </tr>
                            <tr>
                                <th>Average Score</th>
                                <td>2</td>
                            </tr>
                            <tr>
                                <th>Total Connections</th>
                                <td>2</td>
                            </tr>
                            <tr>
                                <th>Average SS Points / Connection</th>
                                <td>2</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.tab-pane -->
        
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    function filters(){

        $.ajax({
            url: '/home/user_filters',
            type: 'post',
            data: $('#filters_form').serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            //dataType: 'json',
            success: function (data) {
                console.info(data);
                $('#tab_3').html(data);
            }
        });
    };
</script>
<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Scores', 'Actions'],
          ['-3', 2],
          ['-2', 0],
          ['-1', 2],
          ['0', 0],
          ['1', 1],
          ['2', 3],
          ['3', 4]
        ]);

        var options = {
          title: 'Your Resume',
          width: $(window).width()*0.85,
          height: $(window).height()*0.55,
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
        
</script>
<script>
    function liked(id_action, id_user, status) {
        console.log('status: '+status);
        console.log('id_Action: '+id_action);
        console.log('id_user: '+id_user);
        if(status==0){
            $('#like-'+id_action).removeClass( "fa fa-heart-o" ).addClass('fa fa-heart');
            $.post( "{{ url('api/v1/userlikes') }}", { action_id: id_action, user_id: id_user } ).done(function( data ) {
                console.log( "Data Loaded: " + JSON.stringify(data) );
                var total_likes = parseInt($('#total_likes').val())+1;
                $('#s_total_likes-'+id_action).html(total_likes + ' likes');
              }, "jsonp");
            }else{
                $('#like-13').removeClass( "fa fa-heart" ).addClass('fa fa-heart-o');
                $.ajax({
                    url: "{{ url('api/v1/userlikes') }}/"+status,
                    type: 'DELETE',
                    success: function(result) {
                        console.log( "Data Loaded: " + JSON.stringify(result) );
                        var total_likes = parseInt($('#total_likes').val())-1;
                        $('#s_total_likes-'+id_action).html(total_likes + ' likes');
                    }
                });
        }
        
    }

    {{-- function filters() {
        $.post( '/home/user_filters', {
            '_token': $('meta[name=csrf-token]').attr('content'),
            task: 'comment_insert',
            userID: _userID,
            comment: _comment,
            name: _name,
            userName: _userName
        } $('#filters_form').serialize(), function(data) {
                 console.log(data);
                 $('#tab_3').html(data);
            },'json' // I expect a JSON response
        ); --}}
</script>
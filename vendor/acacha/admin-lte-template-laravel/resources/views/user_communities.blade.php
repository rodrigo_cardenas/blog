<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
@section('htmlheader_title', 'Communities')
    @include('adminlte::layouts.partials.htmlheader')
@show


<body class="skin-green sidebar-mini">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @section('contentheader_title', '')
        @section('contentheader_description', '')
        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    <h3 class="box-title">My Communities</h3>

                    <div class="box-tools">
                        <a data-toggle="modal" data-target="#modal-community" class="btn btn-default" ><i class="fa fa-plus"></i>New Community</a>
                        {{--  <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                        </div>  --}}
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                    <table class="datatable table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Joined At</th>
                                <th>Created by</th>
                                <th>Type</th>
                                <th>Category</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($communities as $key=>$community)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td><a href="{{ url('/community/detail') }}/{{ $community->id }}">{{ $community->name }}</a></td>
                                    <td>{{ $community->joined_at }}</td>
                                    <td><a href="{{ url('/actions/contact') }}/{{ $community->user_id }}/{{ $community->id }}">{{ $community->first_name }} {{ $community->last_name }}</a></td>
                                    <td>{{ ($community->type==1) ? 'Public' : 'Private'  }}</td>
                                    <td>{{ $community->title }}</td>
                                    <td>
                                        <a href="{{ url('/actions/delete') }}/{{ $community->id }}"  title="Leave Community" class="btn btn-default "><i class="fa fa-user-times"></i></a>
                                        <a style="display:{{ ($community->user_id==Auth::id()) ? '' : 'none' }}" onclick="return confirm('Are you sure you want to delete this community?')" href="{{ url('/communities/delete',$community->id) }}" class="btn btn-default" title="Delete"><i class="fa fa-trash"></i></a>
                                        <a data-toggle="modal" data-target="#modal-link-{{ $community->id }}"  class="btn btn-default" title="Invitation Link"><i class="fa fa-link"></i></a>
                                        {{--  new contact modal  --}}
                                        <div class="modal fade" id="modal-link-{{ $community->id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Copy this link to send to your friends</h4>
                                                </div>
                                                <div class="modal-body"><!-- The text field -->
                                                    <input type="text" class="form-control" value="{{url('community/join')}}/{{ $community->id }}" id="myInput-{{ $community->id }}">
                                                    
                                                    <!-- The button used to copy the text -->
                                                    <button onclick="myFunction({{ $community->id }})">Copy text</button>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                        </div>
                                        <!-- /.modal -->
                                    </td>
                                </tr>
                                @endforeach
                                @forelse ($communities as $community)
                                @empty
                                    <p>No actions</p>
                                @endforelse
                        </tbody>
                    </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                </div>
            </div>
            {{--  new contact modal  --}}
            <div class="modal fade" id="modal-community">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">New Community</h4>
                    </div>
                    <div class="modal-body">
                            {!! Form::open(['class'=>'form-horizontal', 'route' => ['community.store'], 'method' => 'post']) !!}
                            <div class="form-group">
                              <input type="hidden" name="view" value="1">
                              <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                              <input type="hidden" name="created_by" value="{{ Auth::id() }}">
                              <label for="inputName" class="col-sm-2 control-label">Name *</label>
                              <div class="col-sm-10">
                                    <input type="text" class="form-control pull-right" name="name" id="name">
                              </div>
                            </div>
                            
                            <div class="form-group">
                              <label for="inputLastName" class="col-sm-2 control-label">Description</label>
          
                              <div class="col-sm-10">
                                  <textarea class="form-control" name="description" id="" cols="30" rows="3"></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputLastName" class="col-sm-2 control-label">Category</label>
          
                              <div class="col-sm-10">
                                  <select class="form-control pull-right" name="category" id="">
                                      @foreach ($CommunityCategoriess as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputLastName" class="col-sm-2 control-label">Invite Contact </label>
          
                              <div class="col-sm-10">
                                  <select name="invite[]" id="" class="select2" multiple>
                                    @foreach ($contacts as $contact)
                                    <option value="{{ $contact->email }}">{{ $contact->first_name }} {{ $contact->last_name }}</option>
                                    @endforeach
                                  </select>
                              </div>
                            </div>
                            
                            <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                              <input id="type" name="type" type="checkbox"  checked> Public (Anyone Can Join)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                            </div>
                          {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                    </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
            <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            
            
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
<script>
    function notesticky(id) {
        $('#user_id').val({{Auth::id()}});
        $('#action_id').val(id);
        console.log('id: '+id);
    }
</script>
<script>
        function myFunction(id) {
            var copyText = document.getElementById("myInput-"+id);
            copyText.select();
            document.execCommand("copy");
        }
</script>
//floating button 
@section('floating_button')
    @include('adminlte::layouts.floating_button')
@show
</html>

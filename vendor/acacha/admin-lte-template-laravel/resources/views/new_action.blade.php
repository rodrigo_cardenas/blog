<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
@section('htmlheader_title', 'Connections')
    @include('adminlte::layouts.partials.htmlheader')
@show




<body class="skin-green sidebar-mini">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('adminlte::layouts.partials.mainheader')

    @include('adminlte::layouts.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @section('contentheader_title', 'New Action')
        @section('contentheader_description', '')
        @include('adminlte::layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title"></h3>
                            </div>

                            <div class="box-body">
                            {!! Form::open(['class'=>'form-horizontal', 'route' => ['actions.store'], 'method' => 'post']) !!}
                           
                            <div class="form-group">
                                <label for="inputLastName" class="col-sm-2 control-label">From </label>
            
                                <div class="col-sm-10">
                                    <select name="contact_id" id="contact_id" class="select2" >
                                      @foreach ($contacts as $contact)
                                      <option value="{{ $contact->id }}">{{ $contact->first_name }} {{ $contact->last_name }}</option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="width:100%">
                                <input type="hidden" name="view" value="1">
                                <input type="hidden" name="user_id" id="user_id" value="{{ Auth::id() }}">
                                <input type="hidden" name="date" id="date" required>
                                <label for="inputName" class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea name="description" id="description" class="form-control" style="margin-left:5px;" rows="2" placeholder=" ..."></textarea>
                                </div>
                              </div>
                            <div class="form-group">
                              <label for="inputLastname" class="col-sm-2 control-label">Date</label>
                              <div class="col-sm-10">
                                <div class="input-group">
                                        <input type="text" name="date_value" id="date_value" required class="form-control datepicker">
                                    
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        
                                        <input type="text" value="0:00" name="time_date" id="time_date" required class="form-control timepicker">
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="inputLastname" class="col-sm-2 control-label">Time Duration</label>
                              <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" name="total_time" value="0:30" required id="total_time" class="form-control timepicker">
                
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail" class="col-sm-2 control-label">Money</label>
          
                              <div class="col-sm-10">
                                <input type="number" value="0" class="form-control" required name="total_money" id="total_money" id="inputMoney" placeholder="Money">
                              </div>
                            </div>
                            
                            <div class="form-group">
                              <label for="inputPhone" class="col-sm-2 control-label">Energy Multiplier</label>
          
                              <div class="col-sm-10">
                                    <select name="energy_multiplier" id="energy_multiplier" required id="inputeEnergy_multiplier" class="form-control">
                                        @foreach ($EnergyMultiplier as $energy_m)
                                        <option value="{!! $energy_m->value !!}">{!! $energy_m->name !!} ({!! $energy_m->value !!})</option>
                                        @endforeach
                                    </select>
                              </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPhone" class="col-sm-2 control-label">Total Giving Amount</label>
                                <div class="col-sm-10">
                                    <input type="number" readonly value="15" required class="form-control" id="total_amount" name="total_giving_amount">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSelfNote" class="col-sm-2 control-label">Self Note</label>
                                <div class="col-sm-10">
                                    <textarea name="self_note" id="self_note" class="form-control" rows="3" placeholder="Add any note here"></textarea>
                                </div>
                            </div>
                            {{--  <div class="form-group">
                                <label for="inputPhone" class="col-sm-2 control-label">Community</label>
            
                                <div class="col-sm-10">
                                      <select name="community_id" id="community_id" class="form-control">
                                          <option value="" selected></option>
                                          @foreach ($Communities as $community)
                                          <option value="{!! $community->id !!}">{!! $community->name !!}</option>
                                          @endforeach
                                      </select>
                                </div>
                              </div>  --}}
                              <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                              <input id="score_now" type="checkbox"> Score Now (Verbal report from connection)
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group" id="div_score" style="display:none;">
                                <label for="inputPhone" class="col-sm-2 control-label">Score</label>
                                <div class="col-sm-10">
                                    
                                    @foreach ($scores as $score)
                                        <button onclick="score_check({{ $score->id }});" type="button" title="{{ $score->name }}" class="btn btn-app" style="min-width:50px !important;"><i class="fa fa-">{{ $score->emoji }}</i> {{ $score->value }}</button>
                                    @endforeach
                                        <input id="score" name="score" style="display:none" type="text">
                                </div>
                                <div class="col-sm-offset-2 col-sm-10">
                                    <textarea name="contact_comment" class="form-control" id="contact_comment" style="width:100%;" rows="3" placeholder="Comment from your connection here"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Save</button>
                                
                                <select style="margin-left:10px;" name="private" id="private">
                                    <option selected value="0"><i class="fa fa-globe"></i> Public</option>
                                    <option  value="1">Private</option>
                                </select>
                                <span data-toggle="tooltip" title="By default this action will be public, you colud change to private"><i class="fa fa-info-circle fa-lg"></i></span>
                              </div>
                            </div>
                          {!! Form::close() !!}
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        </div>
                    </div>
            
            
            @yield('main-content')
       
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    
    @include('adminlte::layouts.partials.controlsidebar')

    @include('adminlte::layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>

<script> 
    $('[data-toggle="tooltip"]').tooltip(); 
    function score_check(value) {
        $('#score').val(value);
        console.log(value);
    }

    function score_check(value) {
        $('#score').val(value);
        console.log(value);
    }
    function actionNew(id, name) {
        $('#contact_tittle').html(name);
        $('#user_id').val({{Auth::id()}});
        $('#contact_id').val(id);
        console.log('id: '+id);
    }
    $( "#energy_multiplier" ).change(function() {
        var hms = $('#total_time').val();   // your input string
        var a = hms.split(':'); // split it at the colons
        var seconds = (+a[0]) * 60  + (+a[1]) ;
        var seconds_c = seconds * 0.5;  
        var total_money = $('#total_money').val();
        var total_money_c = total_money * 3;  
        var energy_multiplier = $('#energy_multiplier').val();
        var total_amount = (seconds_c + total_money_c) * energy_multiplier;
        
        console.log('seconds*0.5 :'+seconds_c);
        console.log('total_money*3 :'+total_money_c);
        console.log('energy_multiplier :'+energy_multiplier);
        console.log('total_amount :'+total_amount);
        $('#total_amount').val(total_amount);

    });
    $( "#total_money" ).change(function() {
        var hms = $('#total_time').val();   // your input string
        var a = hms.split(':'); // split it at the colons
        var seconds = (+a[0]) * 60  + (+a[1]) ;
        var seconds_c = seconds * 0.5;  
        var total_money = $('#total_money').val();
        var total_money_c = total_money * 3;  
        var energy_multiplier = $('#energy_multiplier').val();
        var total_amount = (seconds_c + total_money_c) * energy_multiplier;
        
        console.log('seconds*0.5 :'+seconds_c);
        console.log('total_money*3 :'+total_money_c);
        console.log('energy_multiplier :'+energy_multiplier);
        console.log('total_amount :'+total_amount);
        $('#total_amount').val(total_amount);

    });
    $( "#total_time" ).change(function() {
        var hms = $('#total_time').val();   // your input string
        var a = hms.split(':'); // split it at the colons
        var seconds = (+a[0]) * 60  + (+a[1]) ;
        var seconds_c = seconds * 0.5;  
        var total_money = $('#total_money').val();
        var total_money_c = total_money * 3;  
        var energy_multiplier = $('#energy_multiplier').val();
        var total_amount = (seconds_c + total_money_c) * energy_multiplier;
        
        console.log('seconds*0.5 :'+seconds_c);
        console.log('total_money*3 :'+total_money_c);
        console.log('energy_multiplier :'+energy_multiplier);
        console.log('total_amount :'+total_amount);
        $('#total_amount').val(total_amount);

    });

    $('#score_now').change(function() {
        if($(this).is(":checked")) {
            $('#div_score').css("display", '');
        }else{
            $('#div_score').css("display", 'none');
        }
    });

    $('#time_date').change(function() {
        $('#date').val($('#date_value').val() + ' ' + $('#time_date').val());
        console.log($('#date').val());
    });
    $('#date_value').change(function() {
        $('#date').val($('#date_value').val() + ' ' + $('#time_date').val());
        console.log($('#date').val());
    });
</script>
</html>

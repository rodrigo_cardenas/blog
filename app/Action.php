<?php

namespace App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [
        'description', 'date', 'id_category','total_time','total_money','energy_multiplier','user_id','contact_id','status','score','contact_comment','enabled', 'total_giving_amount', 'self_note', 'community_id', 'springship_points', 'private'
    ];
    protected $table = 'actions';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function like()
    {
        return $this->hasMany('App\UserLikes');
    }
    public function contact()
    {
        return $this->belongsTo('App\UsersContacts');
    }

    public function scopeGetJoins($query)
    {
        $user_id = Auth::id();
        return $query
        ->select('*')
        ->addSelect('actions.id as id')
        ->addSelect('actions.date as date')
        ->addSelect('users.email as user_email')
        ->addSelect('user_contacts.email as contact_email')
        ->addSelect('communities.name as community_name')
        ->addSelect('actions.description as description')
        ->addSelect(DB::raw('CONCAT(users.name, " ", users.last_name) as user_name'))
        ->addSelect(DB::raw('CONCAT(user_contacts.first_name, " ", user_contacts.last_name) as contact_name'))
        ->addSelect('actions.date as date')
        ->addSelect(DB::raw("COUNT(user_likes.id) AS total_likes"))
        ->join('user_likes', 'user_likes.action_id', '=', 'actions.id', 'left outer')
        ->join('users', 'actions.user_id','=','users.id')
        ->join('scores', 'actions.score','=','scores.id', 'left outer')
        ->join('user_contacts', 'actions.contact_id','=','user_contacts.id')
        ->join('communities', 'actions.community_id','=','communities.id', 'left outer')
        ->where('actions.enabled',1)
        ->where('actions.private',0)
        ->groupBy('actions.id')
        ->orderBy('actions.id', 'desc')
        ->take(100)
        ->with(['like' => function ($like) use ($user_id) {
            return $like->whereHas('user', function ($user) use ($user_id) {
                $user->where('id', $user_id);
            })->get();
        }]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Action;
use App\Community;
use App\CommunityUsers;
use App\CommunityCategories;
use App\UsersContacts;
use Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class CommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:50',
            'description' => 'required|max:200',
            'user_id' => 'required',
        ];
        // Ejecutamos el validador, en caso de que falle devolvemos la respuesta
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'created' => false,
                'errors'  => $validator->errors()->all()
            ];
        }
        
        if ($request->type == 'on') 
        $request->merge(['type' => 1]);
        else
        $request->merge(['type' => 2]);
        
        $Community = Community::create($request->all());
        $CommunityUsers = CommunityUsers::create([
            'user_id' => $request->user_id,
            'community_id' => $Community->id,
            'privileges' => 3,
        ]);
        if (!empty($request->invite)) {
            $email_chain = implode(",", $request->invite);
            $user = User::find($request->user_id);
            $new_email = MailController::invitation_email($request, $Community->id, $email_chain, $user);
        }
        if ($request->view==1) {
            return back()->withInput();
        } 
        return Response::json(['created' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$community = Community::find($id)) {
            return Response::json(['updated' => false, 'errors' => 'unknown ID'], 200);
        }
        $community_update = Community::find($id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //web:
    public function UserCommunities($id)
    {
        $communities = User::find($id)->communities;
        $contacts = UsersContacts::all()->where('enabled', 1)->where('user_id',$id);
        $CommunityCategoriess = CommunityCategories::all()->where('enabled', 1);
        return view('adminlte::user_communities', ['message' => 'rrt
        ', 'communities'=> $communities, 'contacts'=> $contacts, 'CommunityCategoriess'=> $CommunityCategoriess]);
    }

    public function delete($id)
    {
        if (!$community = Community::find($id)) {
            return Response::json(['deleted' => false, 'errors' => 'unknown ID'], 200);
        }
        $community_update = Community::find($id)->update(['enabled' => 0]);
        return redirect()->back();
    }

    public function new()
    {
        $communities = User::find(Auth::id())->communities;
        $contacts = UsersContacts::all()->where('enabled', 1)->where('user_id',Auth::id());
        $CommunityCategories = CommunityCategories::all()->where('enabled', 1);
        return view('adminlte::new_community', ['message' => 'rrt
        ', 'communities'=> $communities, 'contacts'=> $contacts, 'CommunityCategoriess'=> $CommunityCategories]);
    }

    public function detail($id)
    {
        $communities = Community::getJoins()->where("communities.id",$id)->first();
        $communities_users = CommunityUsers::where('communities_id',$id);
        $CommunityCategories = CommunityCategories::all()->where('enabled', 1);
        return view('adminlte::community_detail', ['communities'=> $communities, 'communitycategories'=> $CommunityCategories]);
    }
}

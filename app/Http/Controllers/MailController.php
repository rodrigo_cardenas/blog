<?php
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use App\Mail\WelcomeEmail;
use App\Mail\ActionEmail;
use App\Mail\InvitationEmail;
use App\Action;
use App\Score;
use Illuminate\Support\Facades\Mail;
 
class MailController extends Controller
{
    public static function send()
    {
        $objDemo = new \stdClass();
        $objDemo->demo_one = 'Demo One Value';
        $objDemo->demo_two = 'Demo Two Value';
        $objDemo->sender = 'SenderUserName';
        $objDemo->receiver = 'ReceiverUserName';
 
        Mail::to("rodrigo.cardenasplaza@gmail.com")->send(new DemoEmail($objDemo));
    }
    
    public function reply($id, $score)
    {
        if (!$action = Action::where('id',$id)->where('enabled', 1)->where('status', 1)->first()) {
            return view('reply', ['message' => 'Sorry, This action is not available
            ']);
        }
        $userContact_update = Action::find($id)->update(['score' => $score, 'status' => 2]);
        //mail de notificación de respuesta
        //Mail::to("rodrigo.cardenasplaza@gmail.com")->send(new DemoEmail($objDemo));
        return view('reply', ['message' => 'Thanks for your Reply!
        ', 'action'=> $action]);
    }

    public function comment(Request $request, $id)
    {
        // var_dump($id);die();
        if (!$action = Action::where('id',$id)->first()) {
            return view('reply', ['message' => 'Sorry, This action is not available
            ']);
        }
        $userContact_update = Action::find($id)->update(['contact_comment' => $request->comment, 'status' => 3]);
        //mail de notificación de comentario
        //Mail::to("rodrigo.cardenasplaza@gmail.com")->send(new DemoEmail($objDemo));
        return view('reply', ['message' => 'Thanks for your Reply!
        ', 'action'=> $action]);
    }

    public static function welcome_email($email_data)
    {
        $objDemo = new \stdClass();
        $objDemo->link = 'springship.com/profile/'.$email_data['email'];
        $objDemo->sender = 'SPRINGSHIP';
        $objDemo->receiver = $email_data['name'] . ' ' . $email_data['last_name'];
 
        Mail::to($email_data['email'])->send(new WelcomeEmail($objDemo));
    }
    
    public static function action_email($action, $id, $user, $contact)
    {
        $objDemo = new \stdClass();
        $objDemo->scores = Score::all();
        $objDemo->id = $id;
        $objDemo->description = $action->description;
        $objDemo->sender = $user->name . ' ' . $user->last_name;
        $objDemo->receiver = $contact->first_name . ' ' . $contact->last_name;
 
        Mail::to($contact->email)->send(new ActionEmail($objDemo));
    }
    public static function invitation_email($community, $id, $email_chain, $user)
    {
        $objDemo = new \stdClass();
        $objDemo->id = $id;
        $objDemo->description = $community->description;
        $objDemo->sender = $user->name . ' ' . $user->last_name;
        $objDemo->receiver = 'Dear';
 
        Mail::to("rodrigo.cardenasplaza@gmail.com")->bcc($email_chain)->send(new InvitationEmail($objDemo));
    }
}
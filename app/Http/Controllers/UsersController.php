<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use App\User;
use App\Action;
use App\UsersContacts;
use App\CommunityUsers;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all()->where('enabled', 1);
        $response = Response::json($users,200);
        return $response; 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users|min:5',
            'name' => 'required|min:2',
            'password' => 'required|min:6',
            'last_name' => 'required|min:2',
            'phone' => 'min:10',
            'avatar' => 'min:1'
        ];
        // Ejecutamos el validador, en caso de que falle devolvemos la respuesta
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'created' => false,
                'errors'  => $validator->errors()->all()
            ];
        }
        $user = User::create($request->all());
        $user->password=bcrypt($request->password);
        $user->save();
        //comprobar pass:
        // if (Hash::check('palqlee', $user->password))
        // {
        //     return Response::json(['created' => true, 'pass', 'eh velda'], 201);
        // }else {
        //     return Response::json(['created' => true, 'pass'=>'eh falaxia'], 201);
        // } 

        //welcome email:
        $new_email = MailController::welcome_email($request);
        return Response::json(['created' => true], 201);
        
    }

    protected function respondWithError($error, $headers = [])
    {
        return Response::json(['errors' => $error])->setStatusCode($this->getStatusCode());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::find($id);
        $response = Response::json($users,200);
        return $response; 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$user = User::find($id)) {
            return Response::json(['updated' => false, 'errors' => 'unknown ID'], 200);
        }
        $user_update = User::find($id)->update($request->except('password'));
        if (isset($request->password)) {
            $rules = [
                'current_password' => 'required',
                'password' => 'required|string|min:6|confirmed',
            ];
            $validator = \Validator::make($request->all(), $rules);
            if (!(Hash::check($request->current_password, $user->password))) {
                return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
            }
            
            if(strcmp($request->current_password, $request->password) == 0){
                return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            }
            
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('password'));
            $user->save();
            return redirect()->back()->with("success","Password changed successfully !");
            
        }
        if ($request->view==1) {
            return back()->withInput();
        } 
        return Response::json(['updated' => true, 'request_data' => $request->all()], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$user = User::find($id)) {
            return Response::json(['deleted' => false, 'errors' => 'unknown ID'], 200);
        }

        $user_delete = User::find($id)->delete();

        return Response::json(['deleted' => true], 200);
    }

    //website:

    //get user profile page
    public function profile($id)
    {
        $user = User::find($id);
        $actions_number = Action::where('user_id', $id)->where('enabled',1)->count();
        $actions_average = round(Action::getJoins()->get()->where('user_id', $id)->avg('value'), 1);
        $springship_points = round(Action::getJoins()->get()->where('user_id', $id)->sum('springship_points'), 1);
        $contacts_number = UsersContacts::where('user_id', $id)->count();
        $comunnity_number = CommunityUsers::getJoins()->get()->where('user_id', $id)->count();
        return view('adminlte::profile', 
        ['message' => 'rrt', 
        'action'=> $user, 
        'actions_number'=> $actions_number, 
        'actions_average'=> $actions_average, 
        'contacts_number'=> $contacts_number, 
        'springship_points'=> $springship_points, 
        'comunnity_number'=> $comunnity_number]);
    }
}

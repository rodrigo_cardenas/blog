<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Action;
use App\UsersContacts;
use App\Score;
use App\EnergyMultiplier;
use App\CommunityUsers;
use Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class ActionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actions = Action::all()->where('enabled', 1);
        $response = Response::json($actions,200);
        return $response; 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'description' => 'required|max:1000',
            'date' => 'date',
            'id_category' => 'min:1',
            'total_time' => 'min:1',
            'total_money' => 'min:1',
            'energy_multiplier' => 'min:1',
            'user_id' => 'required|min:1',
            'contact_id' => 'required|min:1',
            'total_giving_amount' => 'required|min:1',
        ];
        // var_dump($request->total_giving_amount);die();
        // Ejecutamos el validador, en caso de que falle devolvemos la respuesta
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'created' => false,
                'errors'  => $validator->errors()->all()
            ];
        }
        $action = Action::create($request->all());
        
        //action email:
        if ($request->score=='') {
            $user = User::find($request->user_id);
            $contact = UsersContacts::find($request->contact_id);
            $new_email = MailController::action_email($request, $action->id, $user, $contact);
        }else{
            $score_value = Score::find($request->score)->value;
            $action->springship_points = $score_value * $request->total_giving_amount;
            $action->save();
            // var_dump($score_value);
            // var_dump($request->total_giving_amount);
            // var_dump($action->springship_points);
            // die();
        }
        if ($request->view==1) {
            // return back()->withInput();
            return redirect('home');
        } 
        return Response::json(['created' => true, 'request_data' => $request->all()], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $action = Action::find($id);
        $response = Response::json($action,200);
        return $response; 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$user = Action::find($id)) {
            return Response::json(['updated' => false, 'errors' => 'unknown ID'], 200);
        }
        $userContact_update = Action::find($id)->update($request->all());
        

        return Response::json(['updated' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$user = Action::find($id)) {
            return Response::json(['deleted' => false, 'errors' => 'unknown ID'], 200);
        }

        $action_delete = Action::find($id)->delete();

        return Response::json(['deleted' => true], 200);
    }

    public function getUserActions($id)
    {
        $contacts = Action::all()->where('enabled', 1)->where('user_id',$id);
        $response = Response::json($contacts,200);
        return $response;
    }

    //web:
    public function UserActions($id)
    {
        // $actions = Action::all()->where('enabled', 1)->where('user_id',$id);
        $actions = User::find($id)->actions;
        
        // var_dump($actions);die();
        return view('adminlte::user_actions', ['message' => 'rrt
        ', 'actions'=> $actions]);
    }
    public function UserActionsList($id)
    {
        $actions = User::find(Auth::id())->actions;
        $actions2 = Action::getJoins()->get()->where('email',Auth::user()->email)->whereNotIn('user_is',Auth::id());
        $new_collection = $actions2->merge($actions)->sortBy('id', SORT_REGULAR, true);        
        // var_dump($actions);die();
        return view('adminlte::user_actions_list', ['message' => 'rrt
        ', 'actions'=> $new_collection]);
    }
    public function UserHome()
    {
        // $actions = Action::all()->where('enabled', 1)->where('user_id',$id);
        $actions = User::find(2)->actions;
        
        // var_dump($actions);die();
        return view('adminlte::user_actions', ['message' => 'rrt
        ', 'actions'=> $actions]);
    }
    public function FilterActions(Request $request)
    {
        // $actions = Action::all()->where('enabled', 1)->where('user_id',$id);
        if (!empty($request->date)) {
            $dae = explode("-", $request->date);
            $request->merge(['date' => $dae[0]]);
        }
        $actions = User::find()->search($request->all(), 2);
        
        //var_dump($actions);die();
        return view('adminlte::user_home', ['message' => 'rrt
        ', 'actions'=> $actions]);
    }
    public function UserContactActions($id, $id_contact)
    {
        // $actions = Action::all()->where('enabled', 1)->where('user_id',$id);
        $contact = UsersContacts::find($id_contact);
        $actions = Action::all()->where('contact_id', $id_contact)->where('user_id', $id);
        
        // var_dump($actions);die();
        return view('adminlte::user_contactactions', ['message' => 'rrt
        ', 'actions'=> $actions, 'contact'=> $contact]);
    }

    public function delete_action($id)
    {
        if (!$action = Action::find($id)) {
            return Response::json(['updated' => false, 'errors' => 'unknown ID'], 200);
        }
        $userContact_update = Action::find($id)->update(['enabled' => 0]);
        
        return redirect()->back();
    }

    public function updateAction(Request $request)
    {
        $id = $request->id;
        if (!$action = Action::find($id)) {
            return Response::json(['updated' => false, 'errors' => 'unknown ID'], 200);
        }
        // var_dump($request->all());die();
        $action = Action::find($id);
        $action->self_note = $request->self_note;
        $action->save();    
        return redirect()->back();
    }

    public function new()
    {
        $contacts = UsersContacts::all()->where('enabled', 1)->where('user_id',Auth::id());
        $EnergyMultiplier = EnergyMultiplier::all();
        $Communities = CommunityUsers::getJoins()->get()->where('user_id',Auth::id());
        $scores = Score::all();
        // var_dump($Communities);die();
        return view('adminlte::new_action', ['message' => 'rrt
        ', 'contacts'=> $contacts, 'EnergyMultiplier'=> $EnergyMultiplier, 'scores'=> $scores, 'Communities'=> $Communities]);
    }
}


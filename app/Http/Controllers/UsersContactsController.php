<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersContacts;
use App\User;
use App\Score;
use App\Community;
use App\CommunityUsers;
use App\EnergyMultiplier;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class UsersContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = UsersContacts::all()->where('enabled', 1);
        $response = Response::json($users,200);
        return $response; 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'phone' => 'min:7',
            'email' => 'required|email|min:4',
            'user_id' => 'min:1',
        ];
        // Ejecutamos el validador, en caso de que falle devolvemos la respuesta
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'created' => false,
                'errors'  => $validator->errors()->all()
            ];
        }
        $user = User::where('email',$request->email)->first();
        $userContacts = UsersContacts::create($request->all());
        if (isset($user->id)) {
            $userContacts->contact_id = $user->id;
            $userContacts->save();
        }
        if ($request->view==1) {
            return back()->withInput();
        } 
        return Response::json(['created' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = UsersContacts::find($id);
        $response = Response::json($users,200);
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$user = UsersContacts::find($id)) {
            return Response::json(['updated' => false, 'errors' => 'unknown ID'], 200);
        }
        $userContact_update = UsersContacts::find($id)->update($request->all());
        if ($request->view==1) {
            return back()->withInput();
        } 

        return Response::json(['updated' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$user = UsersContacts::find($id)) {
            return Response::json(['deleted' => false, 'errors' => 'unknown ID'], 200);
        }

        $usercontact_delete = UsersContacts::find($id)->delete();

        return Response::json(['deleted' => true], 200);
    }

    public function getUserContacts($id)
    {
        $contacts = UsersContacts::all()->where('enabled', 1)->where('user_id',$id);
        $response = Response::json($contacts,200);
        return $response;
    }

    //web:
    public function UserContacts($id)
    {
        $contacts = UsersContacts::all()->where('enabled', 1)->where('user_id',$id);
        $EnergyMultiplier = EnergyMultiplier::all();
        $Communities = CommunityUsers::getJoins()->get()->where('user_id',Auth::id());
        $scores = Score::all();
        // var_dump($Communities);die();
        return view('adminlte::user_contacts', ['message' => 'rrt
        ', 'contacts'=> $contacts, 'EnergyMultiplier'=> $EnergyMultiplier, 'scores'=> $scores, 'Communities'=> $Communities]);
    }

    public function delete($id)
    {
        if (!$user = UsersContacts::find($id)) {
            return Response::json(['deleted' => false, 'errors' => 'unknown ID'], 200);
        }

        $userContact_update = UsersContacts::find($id)->update(['enabled' => 0]);

        return redirect()->back();
    }

    public function new()
    {
        $contacts = UsersContacts::all()->where('enabled', 1)->where('user_id',Auth::id());
        $EnergyMultiplier = EnergyMultiplier::all();
        $Communities = CommunityUsers::getJoins()->get()->where('user_id',Auth::id());
        $scores = Score::all();
        // var_dump($Communities);die();
        return view('adminlte::new_contact', ['message' => 'rrt
        ', 'contacts'=> $contacts, 'EnergyMultiplier'=> $EnergyMultiplier, 'scores'=> $scores, 'Communities'=> $Communities]);
    }
}

<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use App\Action;
use App\CommunityUsers;
use Carbon\Carbon;
use App\UsersContacts;
use Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $actions = User::find(Auth::id())->actions;
        $actions2 = Action::getJoins()->get()->where('email',Auth::user()->email)->whereNotIn('user_id',Auth::id());
        $new_collection = $actions2->merge($actions)->sortBy('id', SORT_REGULAR, true);
        // var_dump($new_collection);die();
        
        return view('adminlte::user_home', ['message' => 'rrt
        ', 'actions'=> $new_collection]);
    }

    public function global()
    {
        $actions = User::find(Auth::id())->global_actions;
        $actions2 = Action::getJoins()->get()->whereNotIn('user_id',Auth::id());
        // var_dump($actions2);die();
        $new_collection = $actions2->merge($actions)->sortBy('id', SORT_REGULAR, true);
        
        return view('adminlte::layouts.global_tab', ['message' => 'rrt
        ', 'actions'=> $new_collection]);
    }
    
    public function community()
    {
        $actions = User::find(Auth::id())->community_actions;
        $user_communities = CommunityUsers::where('user_id', Auth::id())->get(['community_id'])->toArray();
        $community_array = array();
        foreach ($user_communities as $key => $value) {
            array_push($community_array, $value['community_id']);
        }
        $actions2 = Action::getJoins()->get()->whereNotIn('user_id',Auth::id())->whereIn('community_id', $community_array);
        $new_collection = $actions2->merge($actions)->sortBy('id', SORT_REGULAR, true);
        
        return view('adminlte::layouts.global_tab', ['message' => 'rrt
        ', 'actions'=> $new_collection]);
    }

    public function user_filters(Request $request)
    {
        $actions = User::find(Auth::id())->filter_actions($request->all());
        $actions2 = Action::getJoins()->get()->where('email',Auth::user()->email)->whereNotIn('user_id',Auth::id());
        $new_collection = $actions2->merge($actions)->sortBy('id', SORT_REGULAR, true);
        
        return view('adminlte::layouts.global_tab', ['message' => 'rrt
        ', 'actions'=> $new_collection]);
    }
}

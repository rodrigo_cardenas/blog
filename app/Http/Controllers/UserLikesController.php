<?php

namespace App\Http\Controllers;

use App\UserLikes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class UserLikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $UserLikes = UserLikes::all();
        return response()->json($UserLikes);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $UserLikes = UserLikes::create($request->all());
        return response()->json($UserLikes);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserLikes  $userLikes
     * @return \Illuminate\Http\Response
     */
    public function show(UserLikes $userLikes)
    {
        return response()->json($userLikes);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserLikes  $userLikes
     * @return \Illuminate\Http\Response
     */
    public function edit(UserLikes $userLikes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserLikes  $userLikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserLikes $userLikes)
    {
        $UserLikes = $userLikes->update($request->all());
        return response()->json($UserLikes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserLikes  $userLikes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$user = UserLikes::find($id)) {
            return Response::json(['deleted' => false, 'errors' => 'unknown ID'], 200);
        }

        $action_delete = UserLikes::find($id)->delete();

        return Response::json(['deleted' => true], 200);
    }
}

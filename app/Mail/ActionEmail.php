<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class ActionEmail extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * The demo object instance.
     *
     * @var Action
     */
    public $action;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($action)
    {
        $this->action = $action;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sender@example.com')
                    ->view('mails.action')
                    ->subject('Springship User Requesting Giving Response')
                    ->with(
                      [
                            'link2' => '1',
                      ])
                      ->attach(public_path('/images').'/Springship Image.png', [
                              'as' => 'Springship Image.png',
                              'mime' => 'image/jpeg',
                      ]);
    }
}
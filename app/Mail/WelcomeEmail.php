<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class WelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * The demo object instance.
     *
     * @var Welcome
     */
    public $welcome;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($welcome)
    {
        $this->welcome = $welcome;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('sender@example.com')
                    ->view('mails.welcome')
                    ->subject('Welcome to Sprinship')
                    ->with(
                      [
                            'link2' => '1',
                      ])
                      ->attach(public_path('/images').'/Springship Image.png', [
                              'as' => 'Springship Image.png',
                              'mime' => 'image/jpeg',
                      ]);
    }
}
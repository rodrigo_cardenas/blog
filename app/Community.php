<?php

namespace App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    protected $fillable = [
        'id', 'name', 'category', 'created_by', 'type', 'description','enabled'
    ];
    protected $table = 'communities';

    public function scopeGetJoins($query)
    {
        return $query
        ->select('*')
        ->addSelect('community_categories.title as category_name')
        ->addSelect('communities.description as description')
        ->addSelect('communities.id as id')
        ->join('community_categories', 'community_categories.id', '=', 'communities.category');
    }
}



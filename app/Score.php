<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = [
        'id', 'name', 'value', 'emoji'
    ];
    protected $table = 'scores';
}

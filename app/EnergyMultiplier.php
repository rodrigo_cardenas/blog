<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnergyMultiplier extends Model
{
    protected $fillable = [
        'id', 'name', 'value'
    ];
    protected $table = 'energy_multiplier';
}

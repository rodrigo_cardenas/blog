<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunityUsers extends Model
{
    protected $fillable = [
        'id', 'community_id', 'user_id', 'privileges'
    ];
    protected $table = 'community_users';

    public function scopeGetJoins($query)
    {
        return $query
        ->select('*')
        ->addSelect('community_users.id as id')
        ->join('communities', 'communities.id','=','community_users.community_id')
        ->where('communities.enabled',1)
        ->orderBy('communities.id', 'desc')
        ->take(100);
    }
}

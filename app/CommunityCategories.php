<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunityCategories extends Model
{
    protected $fillable = [
        'id', 'title', 'description', 'enabled'
    ];
    protected $table = 'community_categories';
}

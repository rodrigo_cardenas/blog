<?php

namespace App;
use App\UserLikes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'name','id', 'password', 'created_at','phone','enabled','avatar','last_name','address','birthdate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function actions()
    {
        $user_id = Auth::id();
        return $this
        ->hasMany('App\Action')
        ->select('*')
        ->addSelect('actions.id as id')
        ->addSelect('actions.date as date')
        ->addSelect('users.email as user_email')
        ->addSelect('user_contacts.email as contact_email')
        ->addSelect(DB::raw('CONCAT(users.name, " ", users.last_name) as user_name'))
        ->addSelect(DB::raw('CONCAT(user_contacts.first_name, " ", user_contacts.last_name) as contact_name'))
        ->addSelect(DB::raw("COUNT(user_likes.id) AS total_likes"))
        ->join('user_likes', 'user_likes.action_id', '=', 'actions.id', 'left outer')
        ->join('users', 'actions.user_id','=','users.id')
        ->join('scores', 'actions.score','=','scores.id', 'left outer')
        ->join('user_contacts', 'actions.contact_id','=','user_contacts.id')
        ->where('actions.enabled',1)
        ->groupBy('actions.id')
        ->orderBy('actions.id', 'desc')
        ->take(100)
        ->with(['like' => function ($like) use ($user_id) {
            return $like->whereHas('user', function ($user) use ($user_id) {
                $user->where('id', $user_id);
            })->get();
        }]);
    }
    public function global_actions()
    {
        $user_id = Auth::id();
        return $this
        ->hasMany('App\Action')
        ->select('*')
        ->addSelect('actions.id as id')
        ->addSelect('actions.date as date')
        ->addSelect('users.email as user_email')
        ->addSelect('communities.name as community_name')
        ->addSelect('user_contacts.email as contact_email')
        ->addSelect(DB::raw('CONCAT(users.name, " ", users.last_name) as user_name'))
        ->addSelect(DB::raw('CONCAT(user_contacts.first_name, " ", user_contacts.last_name) as contact_name'))
        ->addSelect(DB::raw("COUNT(user_likes.id) AS total_likes"))
        ->join('user_likes', 'user_likes.action_id', '=', 'actions.id', 'left outer')
        ->join('users', 'actions.user_id','=','users.id')
        ->join('scores', 'actions.score','=','scores.id', 'left outer')
        ->join('user_contacts', 'actions.contact_id','=','user_contacts.id')
        ->join('communities', 'actions.community_id','=','communities.id', 'left outer')
        ->where('actions.enabled',1)
        ->where('actions.private',0)
        ->groupBy('actions.id')
        ->orderBy('actions.id', 'desc')
        ->take(100)
        ->with(['like' => function ($like) use ($user_id) {
            return $like->whereHas('user', function ($user) use ($user_id) {
                $user->where('id', $user_id);
            })->get();
        }]);
    }

    public function community_actions()
    {
        $user_id = Auth::id();
        return $this
        ->hasMany('App\Action')
        ->select('*')
        ->addSelect('actions.id as id')
        ->addSelect('actions.date as date')
        ->addSelect('actions.description as description')
        ->addSelect('communities.name as community_name')
        ->addSelect('users.email as user_email')
        ->addSelect('user_contacts.email as contact_email')
        ->addSelect(DB::raw('CONCAT(users.name, " ", users.last_name) as user_name'))
        ->addSelect(DB::raw('CONCAT(user_contacts.first_name, " ", user_contacts.last_name) as contact_name'))
        ->addSelect(DB::raw("COUNT(user_likes.id) AS total_likes"))
        ->join('user_likes', 'user_likes.action_id', '=', 'actions.id', 'left outer')
        ->join('users', 'actions.user_id','=','users.id')
        ->join('scores', 'actions.score','=','scores.id', 'left outer')
        ->join('communities', 'actions.community_id','=','communities.id', 'left outer')
        ->join('user_contacts', 'actions.contact_id','=','user_contacts.id')
        ->where('actions.enabled',1)
        ->where('actions.private',0)
        ->whereNotNull('actions.community_id')
        ->groupBy('actions.id')
        ->orderBy('actions.id', 'desc')
        ->take(100)
        ->with(['like' => function ($like) use ($user_id) {
            return $like->whereHas('user', function ($user) use ($user_id) {
                $user->where('id', $user_id);
            })->get();
        }]);
    }

    public function filter_actions($filters)
    {
        $user_id = Auth::id();
        $date = explode("-", $filters['date']);
        $from = trim($date[0]); // porción1
        $to = trim($date[1]); // porción2
        // var_dump($from); die();
        return $this
        ->hasMany('App\Action')
        ->select('*')
        ->addSelect('actions.id as id')
        ->addSelect('actions.date as date')
        ->addSelect('users.email as user_email')
        ->addSelect('communities.name as community_name')
        ->addSelect('user_contacts.email as contact_email')
        ->addSelect(DB::raw('CONCAT(users.name, " ", users.last_name) as user_name'))
        ->addSelect(DB::raw('CONCAT(user_contacts.first_name, " ", user_contacts.last_name) as contact_name'))
        ->addSelect(DB::raw("COUNT(user_likes.id) AS total_likes"))
        ->join('user_likes', 'user_likes.action_id', '=', 'actions.id', 'left outer')
        ->join('users', 'actions.user_id','=','users.id')
        ->join('scores', 'actions.score','=','scores.id', 'left outer')
        ->join('user_contacts', 'actions.contact_id','=','user_contacts.id')
        ->join('communities', 'actions.community_id','=','communities.id', 'left outer')
        ->where('actions.enabled',1)
        ->where('actions.private',0)
        ->whereBetween('actions.date', [$from, $to])
        ->groupBy('actions.id')
        ->orderBy('actions.id', 'desc')
        ->take(100)
        ->with(['like' => function ($like) use ($user_id) {
            return $like->whereHas('user', function ($user) use ($user_id) {
                $user->where('id', $user_id);
            })->get();
        }]);
    }

    public function likedByUser()
    {
        $user_id = Auth::id();
        return  $this
        ->hasMany('App\Action')
        ->with(['like' => function ($like) use ($user_id) {
            return $like->whereHas('user', function ($user) use ($user_id) {
                $user->where('id', $user_id);
            })->get();
        }]);
    }

    public function communities()
    {
        return $this
        ->hasMany('App\CommunityUsers')
        ->select('*')
        ->addSelect('communities.id as id')
        ->addSelect('communities.name as name')
        ->addSelect('community_users.created_at as joined_at')
        ->addSelect('users.name as first_name')
        ->join('communities', 'communities.id','=','community_users.community_id', 'left outer')
        ->join('community_categories', 'communities.category','=','community_categories.id', 'left outer')
        ->join('users', 'communities.created_by','=','users.id')
        ->where('communities.enabled','!=',0)
        ->orderBy('communities.id', 'desc');
    }

    public function search(Array $data, $totalPage){
        $historics = $this
        ->hasMany('App\Action')
        ->select('*')
        ->addSelect('actions.id as id')
        ->join('users', 'actions.user_id','=','users.id')
        ->join('scores', 'actions.score','=','scores.id', 'left outer')
        ->join('user_contacts', 'actions.contact_id','=','user_contacts.id')
        ->where('actions.enabled',1)
        ->orderBy('actions.id', 'desc')
        ->where(function ($query) use ($data){
            if(isset($data['date']))
                $query->where('date', $data['date']);
        });
        return $historics;
    }
    // public function contacts()
    // {
    //     return $this->hasMany('App\Action')->join('users', 'actions.user_id','=','users.id');
    // }
}

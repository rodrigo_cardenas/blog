<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLikes extends Model
{
    protected $fillable = [
        'id', 'action_id', 'user_id'
    ];
    protected $table = 'user_likes';

    public function user() 
    { 
        return $this->hasOne('App\User', 'id', 'user_id'); 
    }

}

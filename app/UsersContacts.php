<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersContacts extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'phone','email','user_id','contact_id','enabled'
    ];
    protected $table = 'user_contacts';

    public function user()
    {
        return $this->belongsTo('User');
    }
}

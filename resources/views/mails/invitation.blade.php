
Hi <i>{{ $invitation->receiver }}</i>, 
 

<div>
    <h3>
            {{ $invitation->sender }} is inviting you to join the community {{ $invitation->description }}:
            
            <a href="http://localhost:8888/blog/public/mail/reply/{{ $invitation->id }}" >Join</a>             
    </h3>
</div>
 
Thank you for helping us create a more connected, more giving world,
<br/>
<i>Springship</i>
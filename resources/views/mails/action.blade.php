
Hi <i>{{ $action->receiver }}</i>,
<p>{{ $action->sender }} would like to learn how to connect with you better.  Can you please respond to the action below with an emoticon to help them learn if or how they can do better in the future?  After you click an emoticon, a comment option will be available and would be even more appreciated.</p>
 
 
<div>
<p><b>Description:</b>&nbsp; <{{ $action->description }}</p>
</div>
<div>
    <h3>
            Rate this action please:
            
            @foreach ($action->scores as $score)
            <a href="http://localhost:8888/blog/public/mail/reply/{{ $action->id }}/{{ $score->id }}" title="{{ $score->name }}">{{ $score->emoji }}</a> 
            @endforeach
            
    </h3>
</div>
 
Thank you for helping us create a more connected, more giving world,
<br/>
<i>Springship</i>

Hello <i>{{ $welcome->receiver }}</i>,
<p>Welcome to Springship! </p>
 
<p>You are joining a journey to bring people, communities, and the world in unprecedented ways.  With your help, we are on our way to creating A Unified World That Pursues Love.  We are very excited to welcome you onboard!</p>
<div>
<p><b>Profile link:</b>&nbsp; <a href="{{ $welcome->link }}" target="_blank">Click here</a></p>
</div>
 
Thank You,
<br/>
<i>{{ $welcome->sender }}</i>
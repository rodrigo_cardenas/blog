
Hello <i>{{ $demo->receiver }}</i>,
<p>Welcome to Springship! </p>
 
 
<div>
<p><b>Profile link:</b>&nbsp; <a href="{{ $demo->link }}" target="_blank">Click here</a></p>
</div>
 
Thank You,
<br/>
<i>{{ $demo->sender }}</i>